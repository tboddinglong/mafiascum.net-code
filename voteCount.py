﻿# Standard package imports
import re
from sys import stderr
from optparse import OptionParser as OP
from sets import Set
import codecs

# non-standard imports
import BeautifulSoup as BS
import requests
from nltk.metrics import edit_distance as distance

def main():
    """ script to pull a thread from Mafiascum.net
        count votes & print all deciding vote counts as well as final VC
        :rtype: List of Strings
        :returns: strings without '\n' reflecting the gamestate
    """
    
    # input logic
    parser = OP(usage = 'usage: %prog [options]')
    parser.add_option('-i','--interactive', action='store_true', 
        dest='interactive', default=False,
        help='Manually resolve unrecognized votes')
    parser.add_option('-c', '--config-file', type='string', 
        dest='config_path',default=None, help='Location of config file')
    parser.add_option('-v', '--verbose', action='store_true',
        dest='verbose', default=False,
        help='Output votes as well as vote counts')
    (options, args) = parser.parse_args()
    # Process config file
    try:
        with open(options.config_path) as i:
            config_data = i.readlines()
    except IOError as e:
        # try to be helpful, kill script
        print >> stderr, str(options.config_path) + ' cannot be accessed'
        raise e
    players = _process_config(config_data)
    # iterate through every player and pull out posts with votes & their time
    votes = []
    id_map = {}
    for player in players:
        # means to refer back to names
        id_map[player.id] = player.names
        for url in player.urls:
            soup_stack = [BS.BeautifulSoup(requests.get(url).text)]
            # need to check if there are more pages (over 200 posts)
            soup_stack.extend(_get_pages(soup_stack[0], url))
            for soup in soup_stack:
                # process each page
                posts = [s for s in soup.findAll('div') 
                    if s.get('class') == 'postbody']
                # Pull out vote text, make a tuple (time, playerid, text)
                for post in posts:
                    result = _process_post(post)
                    if result:
                        time, target = result
                        votes.append((time, player.id, target))
    # order the votes by time
    votes = sorted(votes, key=lambda x: x[0])
    vote_count = {}
    # night variable is used to execute night kills after a lynch
    night = 1
    output = []
    # iterate through votes/unvotes keeping a count track as they come
    while votes:
        time, voter, votee = votes.pop(0)
        match = None
        if votee != 'unvote':
             # search the player list for a direct match
            for player in players:
                if votee.lower() in [l.lower for l in player.names]:
                    match = player.id
                    break
            # other means to find who the intended target is
            if not match:
                if options.interactive:
                    # do this by hand
                    print 'input the number of the correct person'
                    print votee
                    # TODO: Quick boolean to turn non-listed input -> None
                    match = raw_input(' '.join(p.id + ':' + ' '.join(p.names)
                        for p in players))
                else :
                    # heuristic means, first check for inclusion
                    for player in players:
                        for name in [l.lower() for l in player.names]:
                            if (votee.lower() in name or 
                                votee.lower().strip(',.') in name):
                                match = player.id
                                break
                    if 2 <= len(votee) <= 4 :
                        # check all the abbreviations for each player
                        for abbrev in player.gen_abbreviations(votee):
                            if match:
                                break
                            for player in players:
                                if (votee.lower() or abbrev) in player.abbreviations:
                                    match = player.id
                                    break
                    # edit distance, will solve some minor spelling errors
                    # above ED 2 or 3 this is somewhat a Hail Mary
                    if not match:
                        best = (None, float("inf"))
                        for player in players:
                            for name in player.names:
                                dist = distance(votee.lower(), name.lower())
                                if dist < len(votee)/2.0 and dist < best[1]:
                                    best = (player.id, dist)
                        match = best[0]
        # with an idea of who the vote is for now, we can now tally the vote
        # remove the current vote from wherever it is
        if match or votee == 'unvote':
            # Unvote.
            for key,value in vote_count.items():
                if voter in value:
                    vote_count[key] = Set(e for e in value if e != voter)
        # add a vote, need None check. match can == pid == 0
        if match is not None:
            try:
                vote_count[match].add(voter)
            except KeyError:
                vote_count[match] = Set([voter])
        # write out the action if verbose was specified
        if options.verbose and votee == 'unvote':
            output.append(_vote_action(id_map[voter], unvote=True))
        elif options.verbose and match is not None:
            output.append(_vote_action(id_map[voter], 
                votee=id_map[match], new_count=vote_count[match]))
        # check for a majority
        try :
            count = len(vote_count[match])
        except KeyError:
            count = 0
        if count >= (len(players)/2.0):
            output.extend(_report(vote_count, id_map,lynch=match))
            # get rid of the lynch and night kills
            players = [p for p in players if (p.id != match 
                and p.death != night)]
            # reset
            night += 1
            vote_count = {}
    if vote_count:
        output.extend(_report(vote_count, id_map))
    return output
    
        
# field for _time
months = {'Jan':'01', 'Feb':'02', 'Mar':'03', 'Apr':'04', 'May':'05', 'Jun':'06',
     'Jul':'07', 'Aug':'08', 'Sep':'09', 'Oct':'10', 'Nov':'11', 'Dec':'12'}
def _time(post):
    ''' helper function to extract the date & time info from a post
        :type post: BeautifulSoup
        :param post: the soup objecet to get the time from
        
        :rtype: string
        :returns: Time expression YYYYMMDDHHMM
    '''
    text = [s for s in post('p') if s.get('class') == 'author'][0].text
    time_exp = re.compile(
        '[a-zA-z]{3} [0-9]{1,2}, [0-9]{4} [0-9]{1,2}:[0-9]{2} (a|p)m')
    time = time_exp.search(text).group(0).split(' ')
    hour,minute = time[-2].split(':')
    time[1] = re.sub('[^0-9]', '', time[1])
    if time[-1] == 'pm':
        # move to military time
        rollover, hour = divmod(int(hour) + 12,24)
        hour = str(hour)
        if rollover:
            time[1] = str(int(time[1]) + 1)
    if time[-1] == 'am' and hour == '12':
        # midnight, day rollover
        hour = '0'
    if len(hour) < 2:
        hour = '0' + hour
    #small alignment change
    time[-2] = hour
    time[-1] = minute
    # other cleanup
    time[0] = months[time[0]]
    if len(time[1]) < 2:
        time[1] = '0' + time[1]
    # combine everything into a single into a single int
    full_time = time[2] + time[0] + time[1] + time[3] + time[4]
    return full_time
    
def _report(record, id_map, lynch=None):
    ''' specific printing method for a votecount
        :type record: {string:Set([string])
        :param record: the current who is voting who record
        :type players: list of Player objects
        :type lynch: string
        :param lynch: id for a lynched player, if this is a lynch VC
        
        :rtype: list of strings
        :returns: lines without '\n' to write corresponding to the VC
    '''
    output = ['']
    if lynch in id_map:
        output.append('%s has been lynched' %('&'.join(id_map[lynch])))
        output.append('')
        output.append('Lynch Vote Count:')
    else:
        output.append('Vote Count:')
    for key,value in record.items():
        if key is not None:
            voter = '&'.join(id_map[key])
            people = ['&'.join(id_map[i]) for i in value]
            output.append('%s\t-\t%s\tTotal: %d' 
                %(voter, ','.join(people), len(value)))
    output.append('')
    return output

def _vote_action(voter, votee=None, new_count=None, unvote=False):
    ''' Make the relevant string to express this vote for a human
        :type voter: List
        :param voter: List of names for the slot who is voting
        :type votee: List
        :param votee: List of names for the slot who is being voted
        :type new_count: int
        :param new_count: The updated count the slot (after the vote)
        :type unvote: boolean
        :param unvote: indicates if this is a vote or just an unvote
        
        :rtype: string
        :returns: assembled string of this action
    '''
    if len(voter) == 1:
        suffix = 's'
    else:
        suffix = ''
    if unvote:
        names = '&'.join(voter)
        return '%s unvote%s' %(names, suffix)
    else:
        voter_names = '&'.join(voter)
        votee_names = '&'.join(votee)
        new_count = len(new_count)
        return ('%s vote%s %s. Votes change: %d to %d' 
            %(voter_names, suffix, votee_names, new_count-1, new_count))
        
def _process_config(raw_data):
    ''' Helper function to process the config file
        
        :type raw_data: List of string
        :param raw_data: contents of a config file
        
        :rtype: list of Player objects
        :returns: Player object for each person specified in the config
                
        :raises: ValueError -- if format fails
    '''
    # Be really helpful with errors here
    format = ''' The valid config format.
        Seperating whitespace outside of handles must be a single tab, '\t'
        [FORUMNO]   [threadID]
        [alignment],[role]  [player1],[...],[playerN]   [Night of Kill]
        EX.
        11	18699
        town,jailkeeper	benoni	2
        .
    '''
    # Base for all urls
    base = r'http://forum.mafiascum.net/viewtopic.php?f='
    try :
        forum_id, game_id = raw_data[0].split('\t')
    except ValueError as e:
        print >> stderr, 'Could not read url data, check formatting'
        print >> stderr, format
        raise e
    # for some on windows File junk here, encoding to remove it
    forum_id = forum_id.decode('ascii', 'ignore')
    game_id = game_id.strip()
    game_url = base + forum_id + '&t=' + game_id
   # build 90% of the url for an iso. 
   # Player ID needs to be slid in for the element that is None
    player_url = [base, forum_id, '&t=', game_id, 
    r'&st=0&sk=t&sd=a&user_select%5B%5D=', None]
    # URLs are set, use the game thread now to find player ids
    front_page = requests.get(game_url)
    soup = BS.BeautifulSoup(front_page.text)
    ids = [s for s in soup('select') if s.get('name') == 'user_select[]'][0]
    id_map = {}
    for element in ids('option'):
        id_map[element.text.decode('utf-8').lower()] = element.get('value')
    # Build player objects
    players = []
    for i,line in enumerate(raw_data[1:]):
        p = Player(i,player_url)
        info = line.split('\t')
        try :
            alignment_data = info[0].strip()
            player_data = info[1].strip()
        except IndexError as e:
            print >> stderr, 'Could not read player data from ' + line
            print >> stderr, format
            raise e
        p.align(alignment_data)
        p.populate(player_data, id_map)
        # minor issue with this code. There are some names that are
        # numbers, but the collision in 1-10 is only possible with
        # the name squared (ie superscript 2). If this arises as
        # an issue this can be fixed.
        try :
            # last element of the info-log will be name or night of NK
            # few names are int-able, fewer are int-able and collide
            p.death = int(info[-1])
        except ValueError:
            p.death = -1
        players.append(p)
    return players

def _process_post(post):
    ''' pulls out the vote directive from a post
        :type post: BeautifulSoup
        :param text: Soup object corresponding to a forum 'postbody'
        
        :rtype: (string, string)
        :returns: None if no vote found, If vote found, returns time &
            final vote directive of the post, either 'unvote' or the object
    '''
    #expression to catch votes
    # the + on the vote: block catches the common mistake of writing vote
    # in the official vote tag.
    vote_ex = re.compile('(?<!un)(?:vote[: ]+)+(.+)', re.IGNORECASE)
    # remove quotes from the bbcode text
    while post.blockquote:
        sub = post.blockquote
        supress_printing = sub.extract()
    actions = []
    # two Semi-Structured means to indicate a vote, bold & vote tag
    for element in post('span'):
        # look for vote tag
        if element.get('title') == 'This is an official vote.':
            actions.extend(vote_ex.findall(element.text))
        # Implicit unvote with vote makes unvotes only interesting
        # if it is not accompanied by a vote
        elif element.get('title') == 'This is an official unvote.':
            actions.append('unvote')
        # in lieu of votetag, look for bold
        elif element.get('class') == 'noboldsig':
            votes = vote_ex.findall(element.text)
            if votes:
                actions.extend(votes)
            else :
                if 'unvote' in element.text.lower():
                    actions.append('unvote')
                else :
                    pass
    
    if actions:
        return (_time(post), actions[-1])
    return None    

def _get_pages(soup, url):
    ''' find & return soups of pages past the initial
        :type soup: BeautifulSoup
        :param soup: the initial iso-page to look for pages past 1
        :type url: string
        :param url: url used to make the soup object
        
        :rype: list of BeautifulSoup
        :returns: list of all pages given the soup
    '''
    # MS uses relative pathing, need to insert that back
    rel_path = '/'.join(url.split('/')[:3]) + '/'
    # info-display duped, one at top & bottom of page
    pagination = [e for e in soup('div') 
        if e.get('class') == 'pagination'][0]
    pages = filter(None, [e('a') for e in pagination('span')])
    souped_pages = []
    for page in pages:
        # build the link with an exact path
        page_link = page[0].get('href')
        link = re.sub('.', rel_path, page_link, count=1)
        raw = requests.get(link).text
        souped_pages.append(BS.BeautifulSoup(raw))
    return souped_pages
    
class Player(object):
    """ field holder for slots in the game """
    def __init__(self, n, url):
        '''
            :type url: string
            :param url: sharing the iso url for player objects
        '''
        self.iso_url = url
        self.urls = []
        self.mafia_pid = []
        self.names = []
        self.alignment = ''
        self.role = ''
        self.id = n
        self.abbreviations = []
        self.death = None
        
    def align(self, s):
        '''set alignment and role for this Player
            :type s: string
            :param s: alignment,role datum, comma seperated
            
            assumes Vanilla if no role specified
        '''
        sep_data = s.split(',')
        self.alignment = sep_data[0]
        try :
            self.role = sep_data[1]
        except IndexError:
            self.role = 'Vanilla'
            
    def populate(self, raw_names, pid_dict):
        ''' notes the names & pids representing this Player
            will build all of the ISO urls
            :type names: list of strings
            :param names: the names of the players, comma seperate
            :type pid: dictionary
            :param pid: player names mapped to their ids for retrieval
        '''
        for name in raw_names.split(','):
            self.names.append(name)
            self.abbreviations.extend(self.gen_abbreviations(name))
            try:
                id = pid_dict[name.lower()]
                self.mafia_pid.append(id)
                url = self.iso_url
                url[-1] = id
                self.urls.append(''.join(url))
            except KeyError:
                # Continue despite this error
                print >> stderr, ('Could not find ' + name +
                    ' ensure the name is spelled correctly and that ' +
                    'the player has posted in the thread.')
                
    def gen_abbreviations(self, name):
        '''quick heuristic abbreviation model
            :type name: string
            :param name: entity to crunch into an abbreviation
            
            :rtype: list of strings
            :return: the abbreviations
        '''
        abbrev = []
        # Target only caps, leaving off numbers & combination of the two
        abbrev.append(''.join(re.findall('[A-Z]', name)))
        abbrev.append(''.join(re.findall('[^0-9]', name)))
        abbrev.append(''.join(re.findall('[A-Z0-9]', name)))
        abbrev = [e.lower() for e in abbrev if len(e) >= 2]
        return abbrev
    
if __name__ == '__main__':
    for line in main():
        print line