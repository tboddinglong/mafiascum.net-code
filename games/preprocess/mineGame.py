﻿import xml.etree.ElementTree as ET
import requests
from sys import argv, stderr, exit
import codecs
import os
import string
import BeautifulSoup
import re

# basic 'api' link for user-view
base = 'http://forum.mafiascum.net/viewtopic.php?t='
# f=NUM changes based on the forum. 11 = newbie, 50 =  completed newbie
mid = '&f=11&st=0&sk=t&sd=a&user_select%5B%5D='
tail = '&user_select%5B%5D=0&user_select%5B%5D=0&user_sort=Go'

person = [] #going to be tuple ( role, [(name,id)])
# parse loading the basic game file for individual's (or players') IDs & alignment
# For the file, pull in the player, id, game id
info = [i.strip() for i in open(argv[1]).readlines()]
gameID = info[0].split('\t')[1]
# get name -> id mappings, could 1-line this but expanded for readability
topUrl = 'http://forum.mafiascum.net/viewtopic.php?f=50&t=' + gameID
titlepage = requests.get(topUrl)
soup = BeautifulSoup.BeautifulSoup(titlepage.text)('select')
valNameTuples = re.findall(r'value="([0-9]{1,5})">([^<]+)', str(soup[3]).lower())
valNameDict = dict( (t[1],t[0]) for t in valNameTuples )
for playerID in info[1:]:
    # pick apart pieces
    playerID = playerID.split('\t')
    # assign the bits to the right spots
    # assign role-- (basic, advanced)
    role = playerID[0].split(',')
    if len(role) > 1:
        role = (role[0], role[1])
    else:
        role = (role[0], None)
    # assign players & ids, synced lists
    players = []
    for name in playerID[1].split(','):
        name = name.strip()
        try:
            players.append( (name.decode('ascii', 'ignore'), valNameDict[name.lower()] ) )
        except KeyError:
            print >> stderr, "Couldn't find player " + name
    person.append( (role,players) )
    
# Start the XMl
root = ET.Element('game', attrib={'id':gameID})


for p in person:
    # each 'role's' search
    # Make a subelement for this role
    persElement = ET.SubElement(root, 'person', attrib={'align':p[0][0], 'role': str(p[0][1])})
    players = p[1]
    for player in p[1]:
        # each real person who played the role
        name = player[0]
        pid = player[1]
        # load the html for the iso
        soup = BeautifulSoup.BeautifulSoup(requests.get(base + gameID + mid + pid + tail).text)
        postBodies = [p for p in soup.findAll('div') if p.get('class') == 'postbody']
        # process each post
        for chunk in postBodies:
            content = [p for p in chunk('div') if p.get('class') == 'content'][0]
            # remove quote, general assumption is that people don't write in quote boxes
            while content.blockquote:
                sub = content.blockquote
                sub.extract()
            post = content.text     
        # write the posts as a subelement of the role <post user ='blah'> post </post>
            subEl = ET.SubElement(persElement, 'post', attrib={'user':name})
            # Remove control characters, which are breaking the XML
            conOrds = dict.fromkeys(range(32))
            post = post.translate(conOrds)
            try:
                # if encode is wack
                subEl.text = post
            except UnicodeEncodeError:
                post = ''.join([c for c in post if c in string.printable])
                subEl.text = post
# write out the XMLTree
tree = ET.ElementTree(root)
file = os.path.join('..', 'background', argv[1].split('.')[0])
tree.write(file + '.xml', encoding='utf-8')






