﻿"""
	Script to classify ever person's post in a game as :
		Mostly scummy scum .75+
		Somewhat scummy scum .75-
		Somewhat townie town .75-
		Mostly townie town .75+
	and pump out raw nums & proportions of these.
"""

usage = ''' classifyGame gameFile modelFile '''

from sets import Set
import os
import re
from sys import argv
import xml.etree.ElementTree as ET
import subprocess
from nltk import sent_tokenize,word_tokenize

'''This function will find the highest score for a mallet return :'''
def processResult(s) :
    #returns the label with the highest value from a single MALLET result string
    s = s.split()
    scores = {}
    for i in xrange(1,len(s),2) :
        label = s[i]
        value = float(s[i+1])
        scores[label] = value
    best = sorted(scores, key=scores.get, reverse=True)[0]
    return best, scores[best]
    
def tryDiv(x,y):
    """ Function goes around the divide by 0, returns 0.0 in those cases"""
    try:
        return x/y
    except ZeroDivisionError:
        return 0.0

def featurize(text, n=1):
    """ creates a text string of f:v pairs for a provided text string
        arg:
            text    the word-data to be transformed
        kwarg;
            n   the gram-limit for words, default is 1
    """
    if not text :
        return ''
    gramCounts = {}
    while '  ' in text:
        text = re.sub('  ', ' ', text)
    # go through every sentence in the text
    for sentence in sent_tokenize(text):
        words = word_tokenize(sentence)
        for i in xrange(len(words)) :
            for j in range(n,1,-1) :
                # check if the n-gram would roll under the array
                if i-j < 0 :
                    extra = int(abs(j-i))
                    feature = extra * '<s>_' + '_'.join(words[:i])
                # roll over
                elif i+j > len(words):
                    extra = (i+j) - len(words)
                    feature = ('_'.join(words[i:]) + extra * '_<\s>').lower()
                else :
                    #standard
                    feature = '_'.join(words[i-j:i]).lower()
                # fix mallet breakers
                feature = re.sub('[ :#]', '_MALBREAKER_', feature)
                # note this feature
                try :
                    gramCounts[feature] += 1
                except KeyError:
                    gramCounts[feature] = 1
            # add unigrams
            unigram = re.sub('[ :#]', '_MALBREAKER_', words[i])
            try :
                gramCounts[unigram] += 1
            except KeyError:
                gramCounts[unigram] = 1
    return ' '.join([k + ':' + str(v) for k,v in gramCounts.items()])    
    
def run(gamePath, modelPath):    
    # globally things
    tmpfile = os.path.join(os.path.dirname(os.path.abspath(__file__)),'tmp.txt')
    gameLoc, modelLoc = gamePath, modelPath

    # load the XML
    tree = ET.parse(gameLoc)
    root = tree.getroot()

    # Run mallet tests for each person in the game ( 9 calls for newbies, mallet calls can be moved out if its slow)
    i = 0
    o = open(tmpfile, 'w')
    detailedReport = []
    for person in root:
        players = Set([])
        posts = []
        report = []
        for post in person:
            # get the real-world handles these posts attach to
            players.add(post.get('user'))
            posts.append(post.text)
        with open(tmpfile, 'w') as o:
            for text in posts:
                # want to malletproof this text
                print >> o, 'unknown ' + featurize(text,3).encode('ascii', 'ignore')
        malletResults = filter(None, subprocess.check_output(['mallet.bat', 'classify-file', '--input', tmpfile, '--classifier', modelLoc, '--output', '-']).split('\n'))
        os.remove(tmpfile)
        postCount = float(len(person))
        # get the top label for each post's text
        labelledResults = []
        for i in xrange(len(malletResults)):
            label, score = processResult(malletResults[i])
            labelledResults.append( (label, score, posts[i]) )
        # put the results of the classification into human-readable data formats
        scumRes = filter(lambda x: x[0] == 'scum', labelledResults)
        townRes = filter(lambda x: x[0] == 'town', labelledResults)
        # break things roughly into 'strongly' and 'mostly'
        results = [ len( filter(lambda x: x[1] >= .75, townRes ) ),\
                    len( filter(lambda x: .75 > x[1] , townRes) ),\
                    len( filter(lambda x: .75 > x[1], scumRes ) ),\
                    len( filter(lambda x: x[1] >= .75, scumRes ) )]
        # store the data in a form to be printed for each person
        # format [ player(s), CSVrawCounts, CSVproportions, totalPosts, avgScumconfidence, avgTownconfidence, Scummy Posts[(confidence, post)] ]
        report.extend([' '.join(players), ','.join([str(i) for i in results]), ','.join([str(100*(tryDiv(i,postCount)))[:4] for i in results]), str(len(person))])
        #avg scummy #
        report.append( str(100 * tryDiv(sum(f[1] for f in scumRes) , len(scumRes))))
        # avg towny #
        report.append(str(100 * tryDiv(sum(f[1] for f in townRes) , len(townRes))))
        # The scummy posts themselves
        report.extend([str(s[1]) + '\t' + s[2].encode('ascii','ignore') for s in scumRes])
        detailedReport.append(report)
    # spit the data out
    output = []
    basicResults = []
    for entry in detailedReport:
        basicResults.append( ','.join([entry[0],entry[2], entry[3]]) )
    # CSV with proportions
    output.extend(basicResults)
    # bbcode table for proportions
    output.append(table(basicResults))
    # csv with ranks
    output.extend(genRanks(basicResults))
    # bbcode for ranks
    output.append(table(genRanks(basicResults)))
    
    for entry in detailedReport:
        output.append( '\n'.join(entry))
    return output
    
    

def table(output):
#    output = output.split('\n')
    text = []
    text.append( '[table][row][header]' + '[/header][header]'.join(['Name', 'ST','MT', \
            'MS', 'SS', 'Posts']) + '[/header][/row]')
    for line in sorted(output):
        text.append( '[row][header]' + '[/header][header]'.join(line.split(',')) + '[/header][/row]')
    text.append( '[/table]')
    return ''.join(text)
    
def genRanks(resultData):
    """ 
        Arg:
            input format is name, proportions{4},PostCount
            as output by classifyGame.run
        Return:
            input data in same form, but proportions are replaces by relative ranks
    """
    postResults = [i.split(',') for i in resultData]
    # Create complete list of how each person's posts distributed
    SS = [float(i[1]) for i in postResults]
    MS = [float(i[2]) for i in postResults]
    MT = [float(i[3]) for i in postResults]
    ST = [float(i[4]) for i in postResults]
    for i in range(len(postResults)):
        # re-write the data to use ranks rather than raw proportions
        line = postResults[i]
        postResults[i] = ','.join([line[0],rank(line[1],SS),rank(line[2],MS), rank(line[3],MT), rank(line[4],ST), line[5]])
    return postResults
		
	
def rank(n, allNums):
    """ 
        Args:
        n: number to find in list
            allNums: list of floats
        Returns:
            rank of n compared to the other numbers
    """
    # ensure n is a number
    n = float(n)
    if n in allNums:
        return str(len([i for i in allNums if i >= n]))
    else:
        # not in the list, maxrank
        return str(len(allNums))
    
if __name__=="__main__":
    print '\n'.join(run(argv[1], argv[2]))