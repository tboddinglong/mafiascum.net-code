﻿##class specific import
from types import ListType
from TABLkit import *
from sys import stderr

class LanguageModel :
    """
        Language model class to digest a string of text into a series of dictionaries reflecting the contents of the text and their occurrences
    """
    
    def __init__(self, stem=False, caseSensitive=False, window=1):
        """
            args:
                text :: string of text. This is designed with words, not letters, in mind
            kwargs:
                stem  :: default False, True to exhaustively stem tokens before including in LM
                caseSensitive :: default False, True to preserve capital letters
                order :: fedault 1, markov window of the model
        """
        self.caseSens = caseSensitive
        if stem:
            ''' choice of stemmer used is currently to keep capitalization and be aggressive.
            Other stemmers:
            self.stem = nltk.SnowballStemmer().stem #DOES NOT PRESERVE CAPS
            self.stem = nltk.PorterStemmer().stem  
            '''
            self.stem = nltk.LancasterStemmer().stem
        self.docs = []
        self.n = window
    
    def _tokenize_(self,text):
        """ 
           Breaks the text up into nested list: 
            [Paragraph(s)
             [Sentence(s)
              [gram(s)]]]
            Paragraphs are noted by \n characters, blanks excluded
            Returns :: above structure
        """
        paragraphs = filter(None, text.split('\n'))
        document = []
        for p in paragraphs:
            par = []
            for s in nltk.sent_tokenize(p):
                # set up BOS EOS if necessary
                words = ['<s>' for i in range(self.n-1)]
                try:
                    words.extend([stem(w) for w in nltk.word_tokenize(s)])
                except NameError:
                    words.extend([w for w in nltk.word_tokenize(s)])
                words.extend(['</s>' for i in range(self.n-1)])
                par.append(words)
            document.append(par)
        return document
                
    def makeModel(self):
        """
            generates the actual nested dictionaries with counts of occurrences of each gram 
            Dictionaries will all be in the format :
            {Gram: (Count,{})}
        """
        self.LMTop = {}
        for d in self.docs:
            for p in d:
                for sentence in p:
                    # march through the grams, putting them in appropriate dictionaries
                    for i in xrange(self.n-1,len(sentence)):
                        currDict = self.LMTop
                        gramHistory = sentence[i-self.n:i]
                        for gram in gramHistory:
                            # increase seen counts, create new dicts if necessary
                            try:
                                count, tmpDict = currDict[gram]
                                currDict[gram] = (count + 1, tmpDict)
                            except KeyError:
                                currDict[gram] = (1,{})
                            # move a level deeper
                            currDict = currDict[gram][1]
                        # put the gram in the proper level of the dictionary 
                        try:
                            count = currDict[sentence[i]][0]
                            currDict[sentence[i]] = (count + 1, {})
                        except KeyError:
                            currDict[sentence[i]] = (1, {})
                                
    def addDoc(self, text):
        """
            adds a document to the corpus to be processed by this LM
            args:
                text :: human-format text, expects a sole string
        """
        self.docs.append(self._tokenize_(text))